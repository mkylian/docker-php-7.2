FROM php:7.2-cli-stretch

ENV ACCEPT_EULA=Y

RUN apt update -qq && \
    apt install -y -qq lftp zip gzip git && \
    rm -rf /var/lib/apt/* && \
    rm -rf /var/cache/apt/*

RUN apt update -qq && \
    apt install -y -qq libbz2-dev libcurl3-dev libpng-dev libjpeg-dev libldap-dev libmcrypt-dev libzip-dev libxml2-dev openssh-client sshpass rsync && \
    docker-php-ext-configure gd --with-jpeg-dir=/usr/lib/x86_64-linux-gnu/ && docker-php-ext-configure zip --with-libzip && \
    docker-php-ext-install -j$(nproc) pdo pdo_mysql bcmath curl gd json ldap mbstring opcache soap xml xmlrpc zip bz2 intl && \
    apt purge -y -qq libbz2-dev libcurl3-dev libldap-dev libxml2-dev && \
    apt autoremove -y && \
    rm -rf /var/lib/apt/* && \
    rm -rf /var/cache/apt/*

RUN apt update -qq \
    && apt install -y -qq gnupg1 \
    && curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add - \
    && curl https://packages.microsoft.com/config/debian/9/prod.list \
        > /etc/apt/sources.list.d/mssql-release.list \
    && apt-get install -y --no-install-recommends \
        locales \
        apt-transport-https \
    && echo "en_US.UTF-8 UTF-8" > /etc/locale.gen \
    && locale-gen \
    && apt-get update \
    && apt-get -y --no-install-recommends install \
        unixodbc-dev \
        msodbcsql17 && \
    pecl install sqlsrv pdo_sqlsrv && \
    docker-php-ext-enable sqlsrv pdo_sqlsrv && \    
    apt autoremove -y && \
    rm -rf /var/lib/apt/* && \
    rm -rf /var/cache/apt/*



RUN curl --silent --show-error https://getcomposer.org/installer | php
RUN mv composer.phar /usr/local/bin/composer
RUN composer global require hirak/prestissimo

COPY deploy-ftp.sh /usr/local/bin/
COPY deploy-nette-ftp.sh /usr/local/bin/
COPY replace-symlinks.sh /usr/local/bin/
COPY templater /usr/local/bin/
COPY sshx /usr/local/bin/
COPY scpx /usr/local/bin/
COPY rsyncx /usr/local/bin/
